$(document).ready(function() {

    let mobileMenuIcon = $('.jsMobileMenuIcon');
    let mobileMenuClose = $('.jsCloseMobileMenu');
    let mobileMenu = $('.jsMobileMenu');

    mobileMenuIcon.on('click', function() {
        $('body').toggleClass('active');
        $(this).toggleClass('active');
        mobileMenu.toggleClass('active');
        mobileMenuClose.toggleClass('active');
    });

    mobileMenuClose.on('click', function() {
        $('body').toggleClass('active');
        $(this).toggleClass('active');
        mobileMenuIcon.toggleClass('active');
        mobileMenu.toggleClass('active');
    });

    // owl carousel setup and init
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop:true,
        margin:10,
        autoplay:true,
        nav: false,
        dots: true,
        autoplayTimeout: 5000,
        autoplayHoverPause:true,
        autoheight:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            576: {
                items:1
            }
        }
    });

});